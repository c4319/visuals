This sub-project is intended to collect visual and other material to be used for promotional activities. 

The material itself is stored on communitycoins/visuals but can be accessed through links presented here.

## VIDEOS

MP4 and GIF are rendered from an Adobe after-effects source of 500x500 px and a framerate of 30
Rendering means transporting it to an external Video format.
The MP4 is rendered using the YouTube 1080p Full HD requirements

### Youtube Full HD 1080p MP4

- CC version: https://communitycoins.org/img/cc.mp4
- CDN version: https://communitycoins.org/img/cdn.mp4
- AUR version: https://communitycoins.org/img/aur.mp4
- EFL version: https://communitycoins.org/img/efl.mp4

### GIF versions

- CC version: https://communitycoins.org/img/cc.gif
- CDN version: https://communitycoins.org/img/cdn.gif
- AUR version: https://communitycoins.org/img/aur.gif
- EFL version: https://communitycoins.org/img/efl.gif

### Rooty animation

Videos take a lot of space. A 30 sec full HD 1080 MP4 would consume 50MB. AVI about double that amount. A low bandwith solution is to render it using lottie. Through lottiefiles the video source is made available as a json object (just 1MB) and it is rendered on a html5-canvas using javascript:

```
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.9.6/lottie_canvas.min.js" integrity="sha512-89UesjjGg8z9yeEODKvql4RxpS1V1WF62R4sOqG3vf1lJb5fw3ol0R25T0RXZYbwBvtfq8IIky/fTSEQlXmLPA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.9.6/lottie.min.js" integrity="sha512-yAr4fN9WZH6hESbOwoFZGtSgOP+LSZbs/JeoDr02pOX4yUFfI++qC9YwIQXIGffhnzliykJtdWTV/v3PxSz8aw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script>
  var animation = bodymovin.loadAnimation({
    container: document.getElementById('lottie'), 
    path: 'cc.json',
    renderer: 'canvas',
    loop: true, // Optional
    autoplay: true, // Optional
    name: "rooty", // Name for future reference. Optional.
  })
  </script>
```
The json source : https://communitycoins.org/cc.json 

## Pictures

- Communitycoins rooty logo LIGHT 146k : https://communitycoins.org/img/communitycoins_logo_light.png
- Communitycoins rooty logo DARK 147K : https://communitycoins.org/img/communitycoins_logo.png


